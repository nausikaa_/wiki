<h1>Using your own SSL certificates</h1>
<aside class="alert note">The current setup at Feral means you'll need to access custom domains at https://domain.com:<var>port</var>, where <var>port</var> is a port number above 1024.</aside>
<p>This article will show you how to generate and configure custom SSL certificates for domains you've added. You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="/wiki/slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<p>This guide also assumes you're using nginx - please <a href="/wiki/software/nginx">switch to nginx</a> if you haven't already done so. It also assumes you have gone through the steps to <a href="/wiki/slots/custom-domain">configure your custom domain</a>.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#cert-gen">Generating the certificates</a></li>
            <li><a href="#configure">Configuring the nginx config</a></li>
            <li><a href="#port-forwarding">Setting up the port forwarding</a></li>
        </ol>
    </nav>
</details> 

<h2 id="cert-gen">Generating the certificates</h2>
<p>This guide uses <a href="https://www.sslforfree.com/">sslforfree.com</a> to generate the certificates. THat service itself uses Let's Encrypt as a Certificate Authority. Running a Let's Encrypt certificate generator on your slot (thereby doing away with the additional dependency of sslforfree.com is currently impossible as it requires root access.</p>
<p>Once you've navigated to <a href="https://www.sslforfree.com/">sslforfree.com</a>, put the domain to be protected in the field and click on <samp>Create Free SSL Certificate</samp>.</p>
<p>You'll then need to go through a verification procedure. Make sure you read the details on the page (and know that you'll be agreeing to the Let's Encrypt service agreement by continuing), then click on <samp>Manual Verification</samp>. Click the button which appears, <samp>Manually Verify Domain</samp>.</p>
<p>A 7-step list of instructions will appear, but to summarise them:</p>
<ul>
    <li>Create the required directories</li>
    <li>Upload the file to the newly-created <samp>acme-challenge</samp></li>
    <li>Verify by clicking the link provided under step 5.</li>
</ul>
<p>To create the directories, you can run the following (where <var>domain</var> is replaced by the domain directory you created when setting up the custom domain):</p>
<p><kbd>mkdir -p ~/www/<var>domain</var>/public_html/.well-known/acme-challenge</kbd></p>
<p>You can upload then file using an <a href="/wiki/slots/ftp">FTP program</a> before going back to the sslforfree website and clicking the link they provide under their step 5.</p>
<p>Once all this has been done, click on <samp>Download SSL Certificate</samp>. The files will be generated - if you scroll down you can download them by clicking the button, <samp>Download All SSL Certificate Files</samp>. You can then upload the zip file to your slot and extract it with:</p>
<p><kbd>unzip ~/sslforfree.zip -d ~/certs</kbd></p>
<p>If you're going to have multiple certificates and keys for multiple domains you can rename the files to make things easier. The rest of this guide will assume you've not renamed the files.</p>

<h2 id="configure">Configuring the nginx config</h2>
<p>You'll need a custom config in <samp>~/.nginx/conf.d/</samp> for your domain in order to add the custom SSL details. Here is an example copy you can modify and paste in:
<pre><samp>server {
  listen      8080;
  listen      8181 ssl;
  ssl         on;
  ssl_certificate     <var>home_path</var>/certs/certificate.crt;
  ssl_certificate_key  <var>home_path</var>/certs/private.key;
  server_name <var>example.com</var> *.<var>example.com</var>;
  root        <var>server_root</var>;
  index       index.html index.php;

  autoindex            on;
  autoindex_exact_size off;
  autoindex_localtime  on;

  # Pass files that end in .php to PHP
  location ~ .php$ {
      fastcgi_read_timeout 1h;
      fastcgi_send_timeout 10m;

      include      /etc/nginx/fastcgi.conf;
      fastcgi_pass unix:<var>nginx_socket</var>;
  }

  # Deny access to anything starting with .ht
  location ~ /.ht {
      deny  all;
  }

  # Wordpress in the www root
  #
  #location / {
  #        try_files $uri $uri/ /index.php?$args;
  #}

  # Wordpress in a subdirectory
  #
  #location /wordpress {
  #        try_files $uri $uri/ /wordpress/index.php?$args;
  #}
}</samp></pre>

<p>Make the following replacements to the variables in the above config:</p>
<dl>
  <dt><var>home_path</var></dt>
  <dd>The result of the command <kbd>echo $HOME</kbd></dd>
  <dt><var>example.com</var></dt>
  <dd>Your custom domain name</dd>
  <dt><var>server_root</var></dt>
  <dd>The result of the command <kbd>ls -d ~/www/<var>example.com</var>/public_html</kbd></dd>
  <dt><var>nginx_socket</var></dt>
  <dd>The result of the command <kbd>ls ~/.nginx/php/socket</kbd></dd>
</dl>
<p>Once you're done hold <kbd>ctrl</kbd> + <kbd>x</kbd> to save. Press <kbd>y</kbd> to confirm.</p>

<p>Finally reload the nginx configs with the following command:</p>
<p><kbd>/usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf</kbd></p>

<pre><samp>    listen              8181 ssl;
    ssl                 on;
    ssl_certificate     <var>path-to-home</var>/certs/certificate.crt;
    ssl_certificate_key  <var>path-to-home</var>/certs/private.key;
</samp></pre>
<p>Replace <var>path-to-home</var> in the above config with the actual path to your home directory. You can find this with:</p>
<p><kbd>echo $HOME</kbd></p>

<h2 id="port-forwarding">Setting up the port forwarding</h2>
<p>As in the warning at the top of the article, the Feral set up means that port forwarding needs to be set up to make sure everything works correctly. The upshot of this is that your domain will be accessible with a port number in it, rathter than just the clean URL. In other words, it'd be <samp>https://domain.com:15161</samp> rather than plain old <samp>https://domain.com</samp>. There is currently no way to get around this.</p>
<p>Run the following command after changing <var>port</var> to the port you wish to access your domain on:</p>
<p><kbd>mkdir -p ~/.config/feral/ns/forwarding/tcp &amp;&amp; echo 8181 > ~/.config/feral/ns/forwarding/tcp/<var>port</var></kbd></p>
<p>Every five minutes the system will scan your slot and create the forwarded port for you. Once done, you'll be able to access your domain using https://<var>domain</var>:<var>port</var>, where <var>domain</var>is your custom domain and <var>port</var> is the port you specified to be forwarded to port 8181.</p>